# Carona UFPR Politécnico

## Dados referente à criação do app:

* **Aluna responsável:** Yasmin Allanny Calderon
* **Disciplina:** Desenvolvimento Web I
* **Professor:** Alexander Robert Kutzke
* **Curso:** Tecnologia em Análise e Desenvolvimento de Sistemas
* **Universidade Federal do Paraná**



## Sobre a aplicação 

### Tema

Foi desenvolvida uma aplicação para facilitar e organizar a solicitação e compartilhamento de caronas para estudantes da UFPR no campus do Politécnico.

## Funcionalidades implementadas

* Comandos para criação de tabelas no banco de dados;
* Cadastro de novo usuário;
* Sistema de autenticação (login);
* Criação, edição e delete de posts e comentários;
* OBS.: Comandos de criação, edição, delete, cadastro e login possuem interação com o banco de dados mysql.

## Tecnologias utilizadas

* HTML;
* CSS;
* Mysql;
* PHP;
* JS;