<?php
require_once "./../db_functions.php";
require_once "authenticate.php";

$error = false;
$password = $email = "";

if (!$login && $_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["email"]) && isset($_POST["password"])) {

    $conn = connect_db();

    $email = mysqli_real_escape_string($conn, $_POST["email"]);
    $password = mysqli_real_escape_string($conn, $_POST["password"]);
    $password = md5($password);

    $sql = "SELECT id,name,email,password FROM $table_users
            WHERE email = '$email';";

    $result = mysqli_query($conn, $sql);
    if ($result) {
      if (mysqli_num_rows($result) > 0) {
        $user = mysqli_fetch_assoc($result);

        if ($user["password"] == $password) {

          $_SESSION["user_id"] = $user["id"];
          $_SESSION["user_name"] = $user["name"];
          $_SESSION["user_email"] = $user["email"];

          header("Location: /web1/projeto-carona-ufpr");
          exit();
        } else {
          $error_msg = "Senha incorreta!";
          $error = true;
        }
      } else {
        $error_msg = "Usuário não encontrado!";
        $error = true;
      }
    } else {
      $error_msg = mysqli_error($conn);
      $error = true;
    }
  } else {
    $error_msg = "Por favor, preencha todos os dados.";
    $error = true;
  }
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Login</title>
  <link rel="stylesheet" href="../css/global.css">
  <link rel="stylesheet" href="../css/form.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
</head>

<body>
  <header class="header header-pd-1">
    <h1>Login</h1>

    <?php if ($login) : ?>
      <h3>Você já está logado!</h3>

      <?php exit(); ?>
    <?php endif; ?>

    <?php if ($error) : ?>
      <h3 style="color:red;"><?php echo $error_msg; ?></h3>
    <?php endif; ?>
  </header>

  <main class="main">
    <form class="form" action="<?= $_SERVER["PHP_SELF"] ?>" method="post">
      <div class="form-content">
        <label for="email">Email</label>
        <input type="text" placeholder="exemplo@email.com" name="email" id="email" value="<?php echo $email; ?>" required><br>
        <label for="password">Senha</label>
        <input type="password" name="password" id="password" value="" required><br>
        <div class="submit-content">
          <button class="btn" type="submit" name="submit">Entrar</button>
        </div>
      </div>
    </form>
    <a class="btn" href="../index.php"><button>Voltar</button></a>
  </main>
</body>

</html>