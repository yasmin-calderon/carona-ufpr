<?php
require_once "./../db_functions.php";

$error = false;
$success = false;
$name = $email = $grr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["grr"]) && isset($_POST["password"]) && isset($_POST["confirm_password"])) {

    $conn = connect_db();

    $name = mysqli_real_escape_string($conn, $_POST["name"]);
    $email = mysqli_real_escape_string($conn, $_POST["email"]);
    $grr = mysqli_real_escape_string($conn, $_POST["grr"]);
    $password = mysqli_real_escape_string($conn, $_POST["password"]);
    $confirm_password = mysqli_real_escape_string($conn, $_POST["confirm_password"]);

    if ($password == $confirm_password) {
      $password = md5($password);

      $sql = "INSERT INTO $table_users
              (name, email, grr, password) VALUES
              ('$name', '$email', $grr, '$password');";

      if (mysqli_query($conn, $sql)) {
        $success = true;
      } else {
        $error_msg = mysqli_error($conn);
        $error = true;
      }
    } else {
      $error_msg = "Senha não confere com a confirmação.";
      $error = true;
    }
  } else {
    $error_msg = "Por favor, preencha todos os dados.";
    $error = true;
  }
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Registro</title>
  <link rel="stylesheet" href="../css/global.css">
  <link rel="stylesheet" href="../css/form.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
</head>

<body>
  <header class="header header-pd-1">
    <h1>Dados para registro de novo usuário</h1>
  </header>

  <div class="information">
    <?php if ($success) : ?>
      <div class="success">
        <h3>Usuário criado com sucesso!</h3>
        <p>
          Seguir para <a href="login.php">login</a>.
        </p>
      </div>
    <?php endif; ?>
    <?php if ($error) : ?>
      <div class="error">
        <h3><?= $error_msg; ?></h3>
      </div>
    <?php endif; ?>
  </div>

  <main class="main m-2">
    <form class="form m-0" action="register.php" method="post">
      <div class="form-content">
        <label for="name">Nome</label>
        <input type="text" id="name" name="name" value="<?php echo $name; ?>" required><br>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" value="<?php echo $email; ?>" required><br>
        <label for="grr">GRR</label>
        <input type="text" id="grr" name="grr" value="<?php echo $grr; ?>" required maxlength="8"><br>
        <label for="password">Senha</label>
        <input type="password" id="password" name="password" value="" required><br>
        <label for="confirm_password">Confirmação da senha</label>
        <input type="password" id="confirm_password" name="confirm_password" value="" required><br>
        <div class="submit-content">
          <button class="btn" type="submit" name="submit">Criar usuário</button>
        </div>
      </div>
    </form>
    <a class="btn" href="../index.php"><button>Voltar</button></a>
  </main>
</body>

</html>