<?php
require_once "./login/authenticate.php";
require_once "./db_functions.php";
require_once "lib/sanitize.php";

$success = false;

$conn = connect_db();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $array = (array) json_decode($_POST["json"]);
  
  if (isset($array["id_post"]) && isset($array["new_content"])) {
    $id = mysqli_real_escape_string($conn, sanitize($array["id_post"]));
    $content = mysqli_real_escape_string($conn, sanitize($array["new_content"]));

    $sql = "UPDATE $table_posts
            SET content = '$content'
            WHERE id=" . $id;

    if (mysqli_query($conn, $sql)) {
      $success = true;
    } else {
      $error_msg = mysqli_error($conn);
      $success = false;
      die($error_msg);
    }
  }

  if (isset($array["id_comment"]) && isset($array["new_content"])) {
    $id = mysqli_real_escape_string($conn, sanitize($array["id_comment"]));
    $content = mysqli_real_escape_string($conn, sanitize($array["new_content"]));

    $sql = "UPDATE $table_comments
            SET content = '$content'
            WHERE id=" . $id;

    if (mysqli_query($conn, $sql)) {
      $success = true;
    } else {
      $error_msg = mysqli_error($conn);
      $success = false;
      die($error_msg);
    }
  }
}
disconnect_db($conn);
die(json_encode(['status' => $success]));