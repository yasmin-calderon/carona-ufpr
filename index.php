<?php
require_once "./login/authenticate.php";
require_once "./db_functions.php";
require_once "lib/sanitize.php";

$error = false;
$success = false;

$conn = connect_db();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["post"])) {

    $post = mysqli_real_escape_string($conn, sanitize($_POST["post"]));

    $sql = "INSERT INTO $table_posts
              (id_user, content) VALUES
              ($_SESSION[user_id], '$post');";

    if (mysqli_query($conn, $sql)) {
      $success = true;
    } else {
      $error_msg = mysqli_error($conn);
      $error = true;
    }
  }

  if (isset($_POST["comment"]) && isset($_POST["id-post"])) {
    $id_post = mysqli_real_escape_string($conn, sanitize($_POST["id-post"]));
    $comment = mysqli_real_escape_string($conn, sanitize($_POST["comment"]));

    $sql = "INSERT INTO $table_comments
              (id_user, id_post, content) VALUES
              ($_SESSION[user_id], '$id_post', '$comment');";

    if (mysqli_query($conn, $sql)) {
      $success = true;
    } else {
      $error_msg = mysqli_error($conn);
      $error = true;
    }
  }
} elseif ($_SERVER["REQUEST_METHOD"] == "GET") {
  if (isset($_GET["action"]) && isset($_GET["id"])) {
    $sql = "";
    $id = mysqli_real_escape_string($conn, sanitize($_GET["id"]));

    if ($_GET["action"] == "delete-post") {
      $sql = "DELETE FROM $table_comments WHERE id_post=" . $id;
      if (!mysqli_query($conn, $sql)) {
        die("Problemas para executar ação no BD! <br>" . mysqli_error($conn));
      }
      $sql = "DELETE FROM $table_posts WHERE id=" . $id;
    }
    if ($_GET["action"] == "delete-comment") {
      $sql = "DELETE FROM $table_comments WHERE id=" . $id;
    }

    if ($sql != "") {
      if (!mysqli_query($conn, $sql)) {
        die("Problemas para executar ação no BD! <br>" . mysqli_error($conn));
      }
    }
  }
}

$sql = "SELECT $table_users.name, $table_users.grr, $table_posts.id, $table_posts.id_user, $table_posts.created_at, $table_posts.content 
        FROM $table_users, $table_posts 
        WHERE $table_users.id = $table_posts.id_user
        ORDER BY $table_posts.created_at DESC;";
if (!($posts = mysqli_query($conn, $sql))) {
  die("Problemas para carregar os posts do BD! <br>" . mysqli_error($conn));
}

$sql = "SELECT $table_users.name, $table_comments.id_post, $table_comments.created_at, $table_comments.content, $table_comments.id_user, $table_comments.id
        FROM $table_users, $table_comments
        WHERE $table_users.id = $table_comments.id_user
        ORDER BY $table_comments.created_at ASC;";
if (!($comments = mysqli_query($conn, $sql))) {
  die("Problemas para carregar os comentários do BD! <br>" . mysqli_error($conn));
}

$comments = mysqli_fetch_all($comments);

disconnect_db($conn);
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Carona UFPR</title>
  <script defer src="script.js"></script>
  <link rel="stylesheet" href="./css/global.css">
  <link rel="stylesheet" href="./css/index.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
</head>

<body>
  <header class="header">
    <?php if ($error) : ?>
      <span><?php echo $error_msg; ?></span>
    <?php endif; ?>

    <div class="text">
      <h1>Boas vindas ao Carona UFPR POLITEC
        <?php
        if ($login) {
          echo ", $user_name!";
        } else {
          echo "!";
        }
        ?>
      </h1>
    </div>
    <img src="https://cdn.discordapp.com/attachments/568659426987343892/967547481791541288/cart.gif" alt="Imagem animada de um carro">
  </header>

  <main class="main main-index">
    <?php if ($login) : ?>
      <a class="btn" href="./login/logout.php">Logout</a>
      <div class="create-post-container">
        <form class="form-create-post" action="index.php" method="POST">
          <textarea name="post" placeholder="Precisa de uma carona ou de passageiros?"></textarea>
          <div class="submit-content">
            <button class="btn btn-create-post" type="submit" name="submit-post">Postar</button>
          </div>
        </form>
      </div>
      <div class="posts-container">
        <?php if (mysqli_num_rows($posts) == 0) : ?>
          <span>Nenhum post compartilhado :c</span>
        <?php else : ?>
          <?php while ($post = mysqli_fetch_assoc($posts)) : ?>
            <div class="post-container">
              <div class="post-content">
                <div class="post-info">
                  <div class="user">
                    <p><strong><?= $post["name"] ?></strong></p>
                  </div>
                  <div>
                    <span><?= $post["grr"] ?></span>
                    <span>•</span>
                    <span><?= $post["created_at"] ?></span>
                  </div>
                </div>
                <div class="post-text">
                  <?php if ($_SESSION["user_id"] == $post["id_user"]) : ?>
                    <input type="text" id="id-input-post-<?= $post["id"] ?>" disabled value="<?= $post["content"] ?>">
                    <div class="interactions">
                      <button class="btn-interactions edit" data-id_post="<?= $post["id"] ?>" id="btn-post-edit">Editar</button>
                      <a class="btn-interactions delete" href="<?= $_SERVER["PHP_SELF"] . "?id=" . $post["id"] . "&" . "action=delete-post" ?>">
                        <button id="btn-delete-post">Excluir</button>
                      </a>
                    </div>
                  <?php else : ?>
                    <p><?= $post["content"] ?></p>
                  <?php endif ?>
                </div>
                <div class="comments-container">
                  <h3>Comentários</h3>
                  <form class="post-comments" action="<?= $_SERVER["PHP_SELF"] ?>" method="POST">
                    <textarea name="comment" cols="30" rows="10" placeholder="Escreva um comentário..."></textarea>
                    <input type="hidden" name="id-post" value="<?= $post["id"] ?>">
                    <input class="btn btn-submit-comment" type="submit" name="submit-post" value="comentar">
                  </form>
                  <div class="comments-content">
                    <?php foreach ($comments as $comment) : ?>
                      <?php if ($comment[1] == $post["id"]) : ?>
                        <div class="comment-content">
                          <span class="user-comment"><?= $comment[0] ?></span>
                          <?php if ($_SESSION["user_id"] == $comment[4]) : ?>
                            <input type="text" id="id-input-comment-<?= $comment[5] ?>" disabled value="<?= $comment[3] ?>">
                            <div class="interactions">
                              <button class="btn-interactions edit" data-id_comment="<?= $comment[5] ?>" id="btn-comment-edit">Editar</button>
                              <a class="btn-interactions delete" href="<?= $_SERVER["PHP_SELF"] . "?id=" . $comment[5] . "&" . "action=delete-comment" ?>">
                                <button id="btn-delete-comment">Excluir</button>
                              </a>
                            </div>
                          <?php else : ?>
                            <p><?= $comment[3] ?></p>
                          <?php endif; ?>
                        </div>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
    <?php else : ?>
      <div class="login-container">
        <a class="btn" href="./login/login.php"><button>Login</button></a>
        <a class="btn" href="./login/register.php"><button>Registrar-se</button></a>
      </div>
    <?php endif; ?>
  </main>
</body>

</html>