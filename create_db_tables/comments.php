<?php
require_once '../db_credentials.php';

// Create connection
$conn = mysqli_connect($servername, $username, $db_password);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Choose database
$sql = "USE $dbname";
if (mysqli_query($conn, $sql)) {
    echo "<br>Database changed successfully<br>";
} else {
    echo "<br>Error changing database: " . mysqli_error($conn);
}

// sql to create table
$sql = "CREATE TABLE $table_comments (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  id_user INT(6) UNSIGNED NOT NULL,
  id_post INT(6) UNSIGNED NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at DATETIME,
  content TEXT NOT NULL,
  CONSTRAINT fk_id_user_comment FOREIGN KEY (id_user) REFERENCES $table_users (id),
  CONSTRAINT fk_id_post FOREIGN KEY (id_post) REFERENCES $table_posts (id)
)";

if (mysqli_query($conn, $sql)) {
    echo "<br>Table created successfully<br>";
} else {
    echo "<br>Error creating database: " . mysqli_error($conn);
}

mysqli_close($conn)
?>