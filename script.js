const btns_delete_post = document.querySelectorAll('#btn-delete-post');
const btns_delete_comment = document.querySelectorAll('#btn-delete-comment');
const btns_comment_edit = document.querySelectorAll('#btn-comment-edit');
const btns_post_edit = document.querySelectorAll('#btn-post-edit');

btns_delete_post.forEach(btn => {
  btn.addEventListener('click', (event) => {
    if (confirm("Você tem certeza que deseja remover essa publicação?")) {
      return;
    };
    event.preventDefault();
  });
});

btns_delete_comment.forEach(btn => {
  btn.addEventListener('click', (event) => {
    if (confirm("Você tem certeza que deseja remover esse comentário?")) {
      return;
    };
    event.preventDefault();
  });
})

btns_comment_edit.forEach(btn => {
  btn.addEventListener('click', (e) => {
    console.log('oi');
    const textContent = e.target.textContent;
    const id = e.target.dataset.id_comment;
    const input = document.getElementById(`id-input-comment-${id}`);

    if (textContent !== 'confirmar') {
      e.target.textContent = "confirmar";
      input.disabled = false;
    } else {
      e.target.textContent = "editar";
      input.disabled = true;
      const dataJs = {
        id_comment: id,
        new_content: input.value
      };
      const data = new FormData();
      data.append("json", JSON.stringify(dataJs));
      fetch('http://localhost/web1/projeto-carona-ufpr/edit.php', {
        method: 'POST',
        body: data
      })
        .then(function (response) {
          response.json().then(r => {
            alert(r.status ? 'Comentário atualizado!!' : 'Falha ao atualizar comentário!');
          })
        }).catch(() => {
          alert('Falha ao atualizar comentário!');
        });
    }
  });
});

btns_post_edit.forEach(btn => {
  btn.addEventListener('click', (e) => {
    console.log('oioio');
    const textContent = e.target.textContent;
    const id = e.target.dataset.id_post;
    const input = document.getElementById(`id-input-post-${id}`);

    if (textContent !== 'confirmar') {
      e.target.textContent = "confirmar";
      input.disabled = false;
    } else {
      e.target.textContent = "editar";
      input.disabled = true;
      const dataJs = {
        id_post: id,
        new_content: input.value
      };
      const data = new FormData();
      data.append("json", JSON.stringify(dataJs));
      fetch('http://localhost/web1/projeto-carona-ufpr/edit.php', {
        method: 'POST',
        body: data
      })
        .then(function (response) {
          response.json().then(r => {
            alert(r.status ? 'Comentário atualizado!!' : 'Falha ao atualizar comentário!');
          })
        }).catch(() => {
          alert('Falha ao atualizar comentário!');
        });
    }
  });
});